-- -----------------------------------------------------
-- Schema lokpoll
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `kriya` ;
CREATE SCHEMA IF NOT EXISTS `kriya` DEFAULT CHARACTER SET utf8 ;
USE `kriya` ;

-- -----------------------------------------------------
-- Table `user_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `db_logs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_logs` ;
CREATE TABLE `db_logs` (
  `id` bigint(200) NOT NULL AUTO_INCREMENT,
  `dev` varchar(200) DEFAULT NULL,
  `date` datetime DEFAULT current_timestamp(),
  `scriptName` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user` ;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `gender` varchar(50),
  `imageUrl` varchar(200) DEFAULT NULL,
  `bgImageUrl` varchar(200) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `roleId` bigint(20) DEFAULT NULL,
  `workingStatus` varchar(40) DEFAULT 'inactive',
  `loginStatus` varchar(50) DEFAULT NULL,
  `regDate` datetime DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `lastSeen` datetime DEFAULT NULL,
  `registeredBy` bigint(20) DEFAULT 0,
  `avatarBG` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
);

-- -----------------------------------------------------
-- Table `login_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `login_history`;
CREATE TABLE `login_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) DEFAULT NULL,
  `browserId` bigint(20) DEFAULT NULL,
  `operatingSystem` varchar(100) DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `loginTime` datetime DEFAULT current_timestamp(),
  `logTime` datetime DEFAULT NULL,
  `loginStatus` varchar(40) DEFAULT NULL,
  `browser` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `verification`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `verification`;
CREATE TABLE `verification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `otp` int(11) DEFAULT NULL,
  `sentAt` datetime DEFAULT NULL,
  `verifiedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);

alter table user add column ageRangeId bigint(200);


ALTER TABLE `verification`
CHANGE COLUMN `otp` `otp` VARCHAR(20) NULL DEFAULT NULL ;

alter table user change column workingStatus workingStatus varchar(40) default null;

-- -----------------------------------------------------
-- Table `profile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` varchar(20) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `imageUrl` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `user`
DROP COLUMN `lastName`,
CHANGE COLUMN `firstName` `name` VARCHAR(50) NULL DEFAULT NULL ;

ALTER TABLE `user`
add COLUMN `profession` varchar(40) NULL DEFAULT null,
add COLUMN `company` varchar(40) NULL DEFAULT null,
add COLUMN `latitude` varchar(40) NULL DEFAULT null,
add COLUMN `longitude` varchar(40) NULL DEFAULT null;

-- -----------------------------------------------------
-- Table `hobby`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hobby`;
CREATE TABLE `hobby` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) DEFAULT NULL,
  `hobby` varchar(200) DEFAULT NULL,
  `createdAt` datetime DEFAULT null,
  PRIMARY KEY (`id`)
);

alter table user add column deviceToken varchar(5000);

ALTER TABLE `user`
add COLUMN `audioUrl` varchar(200) DEFAULT NULL;


alter table user add column isTestUser tinyint(4) default 0;
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `address` varchar(500),
  PRIMARY KEY (`id`)
);

alter table user add column appLanguage varchar(100) null;
alter table user add column contentLanguage varchar(100) null;

alter table user add column subscribed tinyint(4) default 0;
